# 5. Group dynamics and final project

## Problem and Objective tree 
The problem and objective tree is a systemic analysis tool that identifies and prioritizes the problems, causes and objectives related to a given project or situation.

The problem tree is used to identify the problems, symptoms and causes that contribute to a main problem. The main problem is first identified, then broken down into sub-problems by asking the question "why does this problem exist?" The sub-problems are then prioritized according to their importance.

The goal tree, on the other hand, is used to identify the objectives and desired outcomes to solve the problems identified in the problem tree. The main objective is first identified, then broken down into sub-objectives by asking the question "why is this objective important?" The sub-objectives are then prioritized according to their importance.

In summary, the problem and objective tree is a tool that allows you to visualize the cause and effect relationships between the different elements related to a given project or situation, and to prioritize these elements according to their importance. This tool is often used in the fields of project management, strategic planning and problem solving.

Here's the tree that have been made by the group :
![](images/projectTree.jpg)

## Group creation :
I chose the [brita filtering bottle](https://www.brita.be/fr_BE/systemes-de-filtration/gourde-filtrante/standard) because it allows to filter the water and to transport it which answers an important climatic stake in the whole world

As I formed the groups, I noticed that many of the participants were concerned about climate issues, as evidenced by their contribution of gourds. So we decided to group together. Thus, our group is made up of Gilles, Jonathan, Jules and Lucie, each with a different background, which will be a real asset for the project.

## Theme and problematic

We started by taking a few minutes to write down words on a piece of paper that were related to our objects. Then, each of us chose a theme that we felt was important and wrote it on a post-it note. We then discussed what motivated us about each theme, before choosing the one we wanted to explore further. The connections that emerged were mainly focused on water, recycling, portability, ...
![](images/brainstorming1.jpeg)

We opted for the following problematic "rethinking the production of an everyday (non-recyclable) object" Based on this problematic, we took turns to write what could follow the sentence "In your place I...".
We then did a tour of the other projects. We wrote down the ideas that the other groups had when they saw our problem. 
Here's the result :
![](images/brainstorming2.jpeg)

## Brainstorming
The brainstorming process was quite difficult because we lacked inspiration for several reasons. We have to make something simple to build, that respects the fablab community, that answers a real ecological problem and that doesn't exist yet.

## Class about group dynamics
![](images/classOrganization.jpeg)
During our discussion on group dynamics, we came across a number of interesting tools and ideas. One of these was the finger technique, which ensures that everyone has an equal chance to speak in a group. Another useful idea was to divide tasks and roles based on each person's skills, breaking down major tasks into smaller subtasks. We used the motivated fingers method to assign roles such as facilitator, secretary, communication lead, and time manager.

We also talked about how meetings should be structured in project management. We suggested a framework that includes an "entry weather" check-in, setting roles for the day, agenda-setting, discussing future roles and tasks. By following this structure, we can ensure that everyone is on the same page and that meetings are productive and efficient.