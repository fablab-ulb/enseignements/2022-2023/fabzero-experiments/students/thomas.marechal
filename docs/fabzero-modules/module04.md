# 4. Microcontrollers
Microcontrollers are compact and cost-effective microcomputers that receive inputs, process them, and produce outputs, typically in the form of electrical currents exchanged with attached peripherals such as sensors, LEDs, and buttons. Microcontroller hardware and software can be customized to solve specific problems. The YD-RP2040 microcontroller, similar to the Raspberry Pi Pico, includes headers for connecting peripherals via cables, an integrated programmable blue LED, and an addressable RGB LED. Programming languages like Arduino, C++, and Python can be used to instruct the microcontroller, with an interpreter converting human-readable code to machine-readable code sent to the chip via the USB-C port.

All operations have been carried out on a Mac. If you're using Windows or Linux, the results may be slightly different.

## Software 
- [Arduino Software](https://www.arduino.cc/en/software)

### IDE 
Here's several reasons why I have chosen the Arduino IDE to code your YD-RP2040 microcontroller:

1. Familiarity: The Arduino IDE is a popular and widely used integrated development environment (IDE) for programming microcontrollers. I had the opportunity to use this IDE in the past with Arduino boards.  
2. Compatibility: The YD-RP2040 is compatible with the Arduino IDE, which means that you can use the same programming language and libraries as for an Arduino board. This makes it easier to find examples and online resources that can help you with your project.  
3. Ease of use: The Arduino IDE is known for its simplicity and ease of use. It has a user-friendly interface and a simplified programming language that can be easily understood even by beginners.  
4. Availability of libraries : Arduino IDE has a large library of pre-written codes that can be easily incorporated into your project. These libraries can save you time and effort by providing you with pre-written code for common tasks such as controlling LEDs or reading sensor data.  

### Board manager 
[Board Manager](https://docs.arduino.cc/software/ide-v2/tutorials/ide-v2-board-manager)
The board manager lets you download the software you need to program a microcontroller.
Here's how to access and download the software for the RP2040 board:
1. Go to "Tools/Board/Boards Manager..." or press "shift command B"
	![](images/Arduino_1.png)
2. Search the type of board that you want to add. For us we will search the RP2040. Click on **INSTALL**
	![](images/Arduino_2.png)

### Library manager
[Arduino libraries](https://docs.arduino.cc/hacking/software/Libraries#)
You may want to use sensors or components such as a touchscreen with your microcontroller.
You can program it from scratch, but there may be an easier way. Some people have already programmed a **library**, so it's easier to code the component in Arduino.

For example, here is the way to download a library for a DHT temperature sensor :
1. Go to "Tools/Manage Libraries..." or press "shift command i"
	![](images/Arduino_4.png)
2. Search for the name of your sensor and choose a library to install.
	![](images/Arduino_5.png)


### Exemple code 
To find an example, go to "File/Examples/".
![](images/Arduino_3.png)
You'll find both basic examples and examples from libraries you've downloaded.

Here is the example of code "Blink" :
```
void setup() {

// initialize digital pin LED_BUILTIN as an output.

pinMode(LED_BUILTIN, OUTPUT);

}

  

// the loop function runs over and over again forever

void loop() {

digitalWrite(LED_BUILTIN, HIGH); // turn the LED on (HIGH is the voltage level)

delay(1000); // wait for a second

digitalWrite(LED_BUILTIN, LOW); // turn the LED off by making the voltage LOW

delay(1000); // wait for a second

}
```


### Run 

You can check that your code is correct and can be uploaded by clicking on the **Verify** button.
![](images/Arduino_6.png)

To upload the code on the board :
1. You have to **Select a Board**. If you just have connect the board to your computer, you'll need to go to "Select other board and port".
	![](images/Arduino_7.png)
2. Next, select your **board** and the **port** to which it is connected to your computer.
	![](images/Arduino_8.png)
3. You can then click on the **Upload** button to send the code to the board.
	![](images/Arduino_9.png)

#### IMPORTANT 
To activate the **boot mode** for the RP2040 board, press the "boot" button while plugging the module into the USB port.
The boot mode may differ from one board to another!

### Good to know
When you're programming, you can get lost in the functions you need to use for your project. Here's a link to [Language Reference] (https://www.arduino.cc/reference/en/) which will help you a lot when you start using Arduino.