# 2. Computer Aided Design (CAD)
CAD (computer-aided design) software is computer software used to create, modify and analyze 2D and 3D models of objects, buildings and products for various industries. It provides a digital representation of the design, which allows for accurate measurement, testing and analysis of the model.
Here are some examples of CAD software:
1. AutoCAD
2. SolidWorks
3. Fusion 360
4. CATIA
5. FreeCAD

## CAD Software Choice 
Here are some key points to explain why I use Fusion360 as my everyday CAD software:  
1. Fusion360 offers an integrated environment for CAD, CAM and CAE, which can lead to a more efficient workflow.
![](images/CAD-CAM-CAE.jpeg)
1. Fusion360 has great collaboration tools that allow you to work on the same project in real time.    
2. Fusion360's user-friendly interface is designed to be accessible and easy to use.    
3. Fusion360 is cloud-based, which allows flexibility to work from anywhere with an internet connection.    
4. Fusion360 offers a free license for non-commercial use, making it accessible to students, hobbyists and small businesses.    

## Objective
Make a flexible lego with a spiral shape using parametrically modelled.
![](images/normalSpiral.png)
![](parameters.png)

Parameters :
- __HoleDiameter__ (mm): diameter of the holes for the lego (shloud be 4.8)
- __DistancBetweenHoles__ (mm): distance between the two holes of the lego (should be 3.2)
- __HoleWallThinkness__ (mm): distance between the face of a hole and the outter face the main lego body
- __GlobalHeight__ (mm): thickness of lego bodies
- __SpiralRevolutions__ (positive integer): number of revolution of the spiral
- __SpiralSize__ (mm): thickness of spiral body

Source : [Lego dimensions][1] [Brick][2] 

## Development
It was 8 years ago when I started 3D modeling using Fusion360. I learned by watching videos on YouTube, which taught me methods that may not have been the best. I developed a creation cycle consisting of three steps that I repeat until I achieve the desired shape.
Steps:
1. Start a sketch:
	I always start the cycle by creating a sketch on a specific side. The goal is to create a 2D drawing that can then be extruded.
	![](images/sketch.png)
	
	Here are some key features of a sketch in Fusion360:
	1. __Constraints__: You can add constraints to your sketch to ensure that your geometry maintains specific relationships and dimensions. This includes geometric constraints, such as perpendicular or parallel lines, and dimensional constraints, such as a line's length or an arc's radius.
	2. __Dimensions__: You can add precise measurements to your sketch by adding dimensions to specific lines, arcs, or other geometric elements.
	3.  __Parameters__: You can add parameters to your sketch to make it more flexible and adaptable. This allows you to change specific dimensions or values and have the entire sketch update accordingly.
	4.  __Sketch Palette__: The Sketch Palette is a tool in Fusion360 that provides a wide range of sketching tools and features, including lines, arcs, circles, rectangles, and more.
	5. ...
1. Extrusion: 
	After having finished my sketch, I extrude the shape. It exists different parameters for an extrusion but it's not important to know more about it as a beginner.
	![](images/extrusion.png)
3. Some Modification on the solid:
	It exists some modifications that can be applied to a solid in Fusion360, here's some :
	1. __Loft__: You can use the Loft tool to create a 3D solid by blending two or more cross-sectional shapes.
	2.  __Fillet__: You can use the Fillet tool to round the corners of a solid, creating a smoother transition between two surfaces.
	3. __Chamfer__: You can use the Chamfer tool to create a bevel on the edges of a solid, creating a flat plane between two surfaces.
	4. __Split__: You can use the Split tool to split a solid into multiple pieces along a plane or a sketch.
	5. ...
4. Repeat from the first step.

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/a76ba7b758064f1fb38543e22979b072/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/3d-models/lego-v6-a76ba7b758064f1fb38543e22979b072?utm_medium=embed&utm_campaign=share-popup&utm_content=a76ba7b758064f1fb38543e22979b072" target="_blank" style="font-weight: bold; color: #1CAAD9;">Lego v6 </a> by <a href="https://sketchfab.com/ThomasMarechal?utm_medium=embed&utm_campaign=share-popup&utm_content=a76ba7b758064f1fb38543e22979b072" target="_blank" style="font-weight: bold; color: #1CAAD9;">ThomasMarechal Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=a76ba7b758064f1fb38543e22979b072" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>

## Licences
Source : [chatGPT][3]

CC Licenses (Creative Commons licenses) are a set of copyright licenses that allow creators to share their work with others under certain conditions. These licenses provide a flexible range of protections and freedoms for creators and users of creative works.

There are six main CC Licenses, each with different levels of permissions:

1.  __CC BY__ (Attribution): Allows others to distribute, remix, adapt, and build upon your work, even commercially, as long as they give you credit.
2.  __CC BY-SA__ (Attribution-ShareAlike): Allows others to distribute, remix, adapt, and build upon your work, even commercially, as long as they give you credit and license their new creations under the same terms.
3.  __CC BY-ND__ (Attribution-NoDerivatives): Allows others to distribute your work, even commercially, as long as they give you credit and don't modify it in any way.
4.  __CC BY-NC__ (Attribution-NonCommercial): Allows others to distribute, remix, adapt, and build upon your work, as long as they give you credit and do not use it for commercial purposes.
5.  __CC BY-NC-SA__ (Attribution-NonCommercial-ShareAlike): Allows others to distribute, remix, adapt, and build upon your work, as long as they give you credit, do not use it for commercial purposes, and license their new creations under the same terms.
6.  __CC BY-NC-ND__ (Attribution-NonCommercial-NoDerivatives): Allows others to distribute your work, as long as they give you credit and do not use it for commercial purposes or modify it in any way.

These licenses are widely used by creators of digital content, such as artists, musicians, photographers, and writers, who want to share their work while retaining some control over how it is used. By using a CC License, creators can encourage others to share and build upon their work, while still protecting their rights as the original creators.

## Checklist du module
 - Parametrically modelled some FlexLinks using 3D CAD software
 - Shown how you did it with words/images screenshots
 - Included your original design files
 - Included the CC license to your work


## References
[1]: <https://commons.wikimedia.org/wiki/File:Lego_dimensions.svg> "Lego dimensions"
[2]: <https://brickipedia.fandom.com/wiki/Brick#:~:text=The%20height%20of%20a%20LEGO,the%20length%20is%208%20millimeters.> "Brick"
[3]: <https://chat.openai.com> "chatGPT"





