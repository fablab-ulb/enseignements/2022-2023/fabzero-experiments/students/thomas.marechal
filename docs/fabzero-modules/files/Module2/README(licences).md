// Files: Lego v6.f3d / Lego v6.fbx / Lego v6.obj / Lego v6.stl

//Author: Thomas Maréchal

// Date: 24 February 2023

// License: Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)