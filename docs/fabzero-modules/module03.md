# 3. 3D printing
I've been interested in 3D printing for a few years now, and have found it to be an incredibly rewarding hobby. Over the years, I've used two different printers: the XYZ Printing printer, which came with its own software, and the Artillery printer, which I used with Cura.  
  
What I learned about 3D printing is that it's a long process of trial and error. There are so many parameters to consider when printing, such as temperature, speed, layer height and fill density. It takes time and patience to learn how to adjust these parameters and get the best results possible.  
  
I have spent countless hours experimenting with different settings and trying to improve my prints. It's frustrating at times, but it's also incredibly satisfying when you finally get a perfect print.  
  
Now, after a few years of experience, I have a much better understanding of how to choose the right settings in cutting software. I have a better idea of which settings are best for different types of printing, and I am able to make adjustments more quickly and efficiently.  
  
Overall, 3D printing is a fascinating and challenging hobby that requires a lot of time and dedication. But with patience and perseverance, it can be incredibly rewarding to see your creations come to life.  

## De  sign parts parameters 
The design parts parameters are pretty simple.
Here's the steps when i open the STL file in Ultimaker Cura : 
1. The orientation of the object can affect the quality of the print. Generally, it is best to orient the object in a way that minimizes overhangs and maximizes support. If the object has a flat base, it is usually best to print it with the base flat against the build plate. It is also very important to consider that the horizontal layer of a 3D printed object is a weak point. Consider it when the object to print is a mechanic part.
![](images/Cura_1.png)
2. In Cura, some predefined profiles are already implemented based on the 3D printing that you are using. 
		For a mechanical part that does not need to be too accurate, I use the standart Quality.
![](images/Cura_2.png)
3. I also use the Skirt option in the Build Plate Adhesion parameter for multiples reasons. But what is the Skirt option ? The skirt option creates a line of material around the perimeter of the print bed, which helps with bed leveling, primes the nozzle, prevents warping, and provides a clean starting point for the actual print. It can be adjusted in terms of the number of lines, distance from the model, and height of the skirt.
![](images/Cura_3.png)
4. And that's it, the model is ready to be printed.
![](images/Cura_4.png)


## My working mechanism 

![](images/workingMechanism.jpeg)

My working mechanism functions similar to the energy-storing component found in clocks, but instead, it utilizes a spiral torsion spring.

## My kit 
I decided to supplement my kit by incorporating a flexlink that belonged to another student.

![](images/kit.jpeg)

