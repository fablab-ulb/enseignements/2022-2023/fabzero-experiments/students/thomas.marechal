# 1. Project management and documentation
GitHub can be very difficult for someone who has never used Git or a terminal before. I remember spending several days getting used to it. However, I discovered a new tool called "Sourcetree" last summer while talking to an engineer during my internship. Sourcetree is a GUI (Graphical User Interface) for Git that can make using it much easier. In this article, I will explain how to install and use Sourcetree.

## Installation
[Download](https://www.sourcetreeapp.com)

## Usage
There are 5 main ideas that are good to know to be able to use the basics of Git. I will briefly explain the purpose of each idea, and then a video will show how to implement them using the application Sourcetree.

### Clone 
In Git, a clone is a copy of a Git repository that is created on a local machine. Cloning a repository allows you to create a local copy of the entire repository, including all its files, branches, and history.

When you clone a Git repository, you create a complete copy of the repository on your local machine, which includes the entire history of the repository. This means you can access and work with all the files and branches available in the original repository, even if you don't have access to the original repository itself.

<iframe src="https://youtu.be/0ejmADYXF7s" width="500" height="380" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://youtu.be/0ejmADYXF7s"> clone video</a>.</p>

### Commit and Push
In Git, a commit is a snapshot of changes made to a file or set of files in a repository. When you commit changes to Git, you create a new version of the file or files that is recorded in the repository history. Each commit is accompanied by a message that describes the changes made in the commit.

The git commit command is used to create a commit in Git. You must first stage the changes you want to commit using the git add command. Once you have staged the changes, you can then run git commit with a message to create a new commit. 

When you commit changes, Git creates a new version of the file with a unique identifier known as a hash. This identifier is used to track changes to the file over time.

After you make one or more commits in Git, you can push them to a remote repository. The git push command is used to upload local changes to a remote repository. When you push commits to a remote repository, they become available to others who have access to the repository.

<iframe src="https://youtu.be/7u-q3TaH7vs" width="500" height="380" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://youtu.be/7u-q3TaH7vs"> commit and push video</a>.</p>

### Branch creation
In Git, a branch is a lightweight, movable pointer to a specific commit in the history of a repository. Branches are used to isolate development work and allow multiple developers to work on different features or parts of a project simultaneously. Creating a branch allows you to work on new features or make changes to existing features without affecting the main code.

<iframe src="https://youtu.be/IQ_YbVB0gL4" width="500" height="380" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://youtu.be/IQ_YbVB0gL4"> branch creation video</a>.</p>

### Merge
In Git, merging is the process of combining changes from one branch into another. Merging allows you to integrate changes made on one branch into another branch so that you have all the changes in one place. This is useful when you have multiple branches in a Git repository and you want to combine changes from one branch into another.

<iframe src="https://youtu.be/eQRBmGqBZjI" width="500" height="380" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://youtu.be/eQRBmGqBZjI"> merge video</a>.</p>

### Pull
In Git, a pull is a command used to retrieve and merge changes from a remote repository into your local repository. When you pull changes, Git automatically downloads the latest changes from the remote repository and attempts to merge them into your local repository. This allows you to keep your local repository up to date with changes made by other developers on the remote repository.

<iframe src="https://youtu.be/DyHXkU1j85U" width="500" height="380" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://youtu.be/DyHXkU1j85U"> pull video</a>.</p>

## Compressing image
- [Video compressor](https://www.veed.io/video-compressor)
- [Photo compressor](https://compressjpeg.com)

## Project management principle
As a computer science student, I can use project management principles to effectively manage my academic projects. Set clear goals, create a project plan, identify and mitigate risks, communicate with my team and stakeholders, track progress, and make adjustments as necessary. These practices will help me complete my projects effectively and gain valuable project management experience.

## How to write effectively ?
I use to do a lot of repport for University and Professional projects. It is important to be efficient but it is also important to provide good quality. I have experience in English but not enough to provide a quality text. I have also the same problem in French. To solve this problem and to show you how to be efficient, I'll explain you how to use [Scribens](https://www.scribens.fr), [DeepL](https://www.deepl.com/translator) and [chatGPT](https://chat.openai.com).

1. Write a text in French or in English (see the paragraph above).
2. Put it into Deepl to correct the language, [Deepl](https://www.deepl.com/translator) will also change the quality of your sentence. If you don't want to change the input-output language, you can simply translate your text from input language to x-language, then from x-language to input language.
	Here is the result : 
	I have had the opportunity to do a lot of reports for academic and professional projects. It is important to be efficient but also to provide good quality. I have experience in English but not enough to provide a quality text. I also have the same problem in French. To solve this problem and show you how to be efficient, I will explain how to use Scribens, DeepL and chatGPT.
3. If you are writting in french you can use [Scribens](https://www.scribens.fr) to correct your text.
4. A new tool I started using this year is [chatGPT](https://chat.openai.com). I ask it to rewrite my text. It gives my text a better structure and allows the reader to better understand the topic.
	Here is the result using this section : """can you rewrite this text to makle it clearer : ## How to write efficiently ? I use to do a lot of repport for University and Professional projects. It is important to be efficient but it is also important to provide good quality. I have experience in English but not enough to provide a quality text. I have also the same problem in French. To solve this problem and to show you how to be efficient, I'll explain you how to use Scribens, DeepL and chatGPT. 1. Write a text in French or in English (see the paragraph above). 2. Put it into Deepl to correct the language, Deepl will also change the quality of your sentence. If you don't want to change the input-output language, you can simply translate your text from input language to x-language, then from x-language to input language. Here is the result : I have had the opportunity to do a lot of reports for academic and professional projects. It is important to be efficient but also to provide good quality. I have experience in English but not enough to provide a quality text. I also have the same problem in French. To solve this problem and show you how to be efficient, I will explain how to use Scribens, DeepL and chatGPT. 3. If you are writting in french you can use [Scribens](https://www.scribens.fr) to correct your text. 4. A new tool I started using this year is chatGPT. I ask it to rewrite my text. It gives my text a better structure and allows the reader to better understand the topic."""

	The answer of chatGPT : Are you looking for a clearer version of this text? Here's my attempt:

	If you often write reports for academic or professional projects, you may be looking for ways to write more efficiently while still producing high-quality work. However, if you're not confident in your language skills, it can be challenging to ensure that your writing is of good quality. To help with this, you can use tools like Scribens, DeepL, and chatGPT.

	To start, write your text in either French or English. Then, use DeepL to correct your language and improve the quality of your sentences. If you prefer not to change the input-output language, you can simply translate your text from the input language to another language and then back again.

	If you're writing in French, you can also use Scribens to correct your text. Another useful tool is chatGPT, which can help you rewrite your text for better structure and clarity, making it easier for your readers to understand.