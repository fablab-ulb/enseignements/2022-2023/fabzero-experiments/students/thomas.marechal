## About me  
  
![](images/profile2.png)

Hello everyone! My name is Thomas Maréchal, and I am currently pursuing my Bachelor's degree in Computer Science at the Université Libre de Bruxelles (ULB) in Belgium. I aspire to specialize in programming languages and computer organization while acquiring knowledge in earth sciences, environment and society.  
  
## My background  
  
I was born and raised in Belgium and attended the Athénée Royal de Huy where I excelled in mathematics (sometimes) and science. I am fluent in English and confident in Spanish (after a few drinks), which I acquired during my schooling there.  
  
## Previous work  
  
In my spare time, I have worked on personal projects including electronics development, 3D modelling and printing, and software development. I also completed an internship at Haption, where I gained experience in virtual reality (VR) and 3D modelling. In addition, during my summer internship at ITER, I honed my skills in VR scene development. I am confident that I can help you bring your visions to life in a VR world.  
  
I have extensive knowledge of programming languages such as Python, C, C#, C++, Bash and SQL. Furthermore, I have experience in using software such as Unity (INTERACT license, PiXYZ license), Autodesk Fusion 360, Choregraphe (SoftBank software), Arduino, Blender and LaTeX.  
  
In my spare time, I enjoy working with robotics and automation. I have a penchant for creating and designing practical and innovative systems. Although I am a computer science student, I am fun and approachable. You can converse with me in English or French, whichever suits you best. Whether you need help with programming or just want to relax with a beer, don't hesitate to contact me.  